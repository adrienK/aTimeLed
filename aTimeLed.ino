#include <Wire.h>
#include <Time.h>
#include <TM1637.h>
#include <DS1307RTC.h>
#include <DeepSleepScheduler.h>

#define CLK 9
#define DIO 8
TM1637 tm1637(CLK,DIO);

// CONFIG
#define LED_DOT 1            //Enable or not dot on led display
#define LED_BRIGHT BRIGHTEST //Bright lvl: BRIGHT_TYPICAL, BRIGHT_DARKEST, BRIGHTEST
#define TIME_SLEEP 6000      //Time beafore sleep mod

void setup() {
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  Serial.begin(9600);
  while (!Serial) ; // wait for serial
  
  tm1637.set(LED_BRIGHT);//BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;
  tm1637.init();
}

void loop() {
  tmElements_t tm;

  if (!RTC.read(tm)) {
    Serial.println("DS1307 read error!  Please check the circuitry.");
    sleep();
  }

  Serial.print(tm.Hour);
  Serial.print(tm.Minute);
  Serial.print(tm.Second);
  Serial.println("");

  tm1637.point(LED_DOT);
  tm1637.display(0, (uint8_t) tm.Hour / 10);
  tm1637.display(1, (uint8_t) tm.Hour % 10);
  tm1637.display(2, (uint8_t) tm.Minute / 10);
  tm1637.display(3, (uint8_t) tm.Minute % 10);

  delay(TIME_SLEEP);
  tm1637.point(0);
  tm1637.clearDisplay();
  sleep();
}

void sleep(void) {
  Serial.println("I go to sleep | Reset me.");
  delay(200);

  scheduler.execute();
}

